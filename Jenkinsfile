pipeline {
  agent any

  triggers {
    cron('H/30 * * * *')
  }

  stages {
    stage('Prepare') {
      steps {
        sh 'docker build --build-arg uid=$(id -u) --build-arg gid=$(id -g) -t tange/auto_test -f docker/Dockerfile .'
      }
    }

    stage('Run for test environment') {
      when {
        branch 'test'
      }

      steps {
        sh 'ln -sf .env.test .env'
        sh 'docker run --rm -v $PWD:/app tange/auto_test'
      }

      post {
        always {
            sh 'rm -rf logs'
            junit 'report.xml'
            allure([
              includeProperties: false,
              jdk: '',
              properties: [],
              reportBuildPolicy: 'ALWAYS',
              results: [[path: 'reports']]
            ])
        }
        failure {
          mail body: "详见 ${env.BUILD_URL}",
               subject: "【测试环境】自动化测试失败：${env.JOB_NAME}#${env.BUILD_NUMBER}",
               to: 'hpf@tange.ai, fh@tange.ai, gbz@tange.ai, yj@tange.ai, fangdijiao@tange.ai'
        }
      }
    }

    stage('Run for production environment') {
      when {
        branch 'master'
      }
      steps {
        sh 'ln -sf .env.prod .env'
        sh 'rm -rf testcases/service/app_work_order_new'
        sh 'rm -rf testcases/service/app_pay_preorder'
        sh 'rm -rf testcases/service/app_service_redeem_code_query_service'
        sh 'rm -rf testcases/service/app_device_report_gps'
        sh 'rm -rf testcases/service/app_car_route_detail'
        sh 'rm -rf testcases/service/app_car_route_current_position'
        sh 'rm -rf testcases/service/app_car_route_list'
        sh 'rm -rf testcases/open_api/app_open_api_car_current_position'
        sh 'rm -rf testcases/open_api/app_open_api_car_route_detail'
        sh 'rm -rf testcases/open_api/device_sd_record_mode_set'
        sh 'docker run --rm -v $PWD:/app tange/auto_test'
      }

      post {
        always {
            sh 'rm -rf logs'
            junit 'report.xml'
            allure([
              includeProperties: false,
              jdk: '',
              properties: [],
              reportBuildPolicy: 'ALWAYS',
              results: [[path: 'reports']]
            ])
        }
        failure {
          mail body: "详见 ${env.BUILD_URL}",
               subject: "【正式环境】自动化测试失败：${env.JOB_NAME}#${env.BUILD_NUMBER}",
               to: 'hpf@tange.ai, fh@tange.ai, gbz@tange.ai, yj@tange.ai, fangdijiao@tange.ai'
        }
      }
    }
  }
}